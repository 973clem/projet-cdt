import { register } from '../../setup.js';

describe('CorrectDisplay', () => {
    it('CorrectDisplay', () => {
        register();

        cy.get('#mat-tab-label-0-1').click();
        cy.get('.button-full').click();
        cy.get('#mat-input-2').should('be.visible');
        cy.get('#mat-input-3').should('be.visible');
        cy.get('#mat-input-4').should('be.visible');
        cy.get('#mat-input-5').should('be.visible');
        cy.get('.justify-content-between > .button-full').should('be.visible');
    })
})