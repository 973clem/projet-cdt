import { register } from '../../setup.js';

describe('Image', () => {
    it('ImageDoesntRespectRegex', () => {
        register();

        cy.get('#mat-tab-label-0-1').click();
        cy.get('.button-full').click();

        cy.get('#mat-input-2').click().type('Test');
        cy.get('#mat-input-3').click().type('bonsoir');
        cy.get('#mat-input-3').invoke('val').then((val) => {
            const regex = new RegExp("^(https?://\\S{0,256})$");
            expect(regex.test(val)).to.be.false;
        });
        cy.get('#mat-input-4').click().type("Testing this ad, don't mind me");
        cy.get('#mat-input-5').click().type('100');
        cy.get('.justify-content-between > .button-full').click();
        cy.get('.error').should('be.visible');
    })
})