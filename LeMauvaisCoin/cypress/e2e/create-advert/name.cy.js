import { register } from '../../setup.js';

describe('Name', () => {
    it('NameDoesntRespectRegex', () => {
        register();
        cy.get('#mat-tab-label-0-1').click();
        cy.get('.button-full').click();

        cy.get('#mat-input-2').click().type('a');
        cy.get('#mat-input-2').invoke('val').then((val) => {
            const regex = new RegExp("^[a-zA-Z0-9 ]{3,30}$");
            expect(regex.test(val)).to.be.false;
        });
        cy.get('#mat-input-3').click().type('https://i.ibb.co/dmQrgXS/biko.jpg');
        cy.get('#mat-input-4').click().type('This is a test');
        cy.get('#mat-input-5').click().type('100');
        cy.get('.justify-content-between > .button-full').click();
        cy.get('.error').should('be.visible');
    })
})