import { register } from '../../setup.js';

describe('Description', () => {
    it('DescriptionDoesntRespectRegex', () => {
        register();
        cy.get('#mat-tab-label-0-1').click();
        cy.get('.button-full').click();

        cy.wait(500);

        cy.get('#mat-input-2').click().type('Test');
        cy.get('#mat-input-3').click().type('https://i.ibb.co/dmQrgXS/biko.jpg');
        cy.get('#mat-input-4').click().type("b");
        cy.get('#mat-input-4').invoke('val').then((val) => {
            const regex = new RegExp("^.{10,256}$");
            expect(regex.test(val)).to.be.false;
        });
        cy.get('#mat-input-5').click().type('100');
        cy.get('.justify-content-between > .button-full').click();
        cy.get('.error').should('be.visible');
    })
})