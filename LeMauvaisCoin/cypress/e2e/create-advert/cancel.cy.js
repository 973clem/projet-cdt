import { register } from '../../setup.js';

describe('ClickOnCancel', () => {
    it('ClickOnCancel', () => {
        register();
        cy.get('#mat-tab-label-0-1').click();
        cy.get('.button-full').click();

        cy.wait(500);

        cy.get('.justify-content-between > .button-border').click();
        cy.get('.button-full').should('be.visible');
    })
})