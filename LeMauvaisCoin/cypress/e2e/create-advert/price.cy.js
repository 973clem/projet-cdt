import { register } from '../../setup.js';

describe('Price', () => {
    it('PriceDoesntRespectRegex', () => {
        register();
        cy.get('#mat-tab-label-0-1').click();
        cy.get('.button-full').click();

        cy.get('#mat-input-2').click().type('Test');
        cy.get('#mat-input-3').click().type('https://i.ibb.co/dmQrgXS/biko.jpg');
        cy.get('#mat-input-4').click().type("Testing this ad, don't mind me");
        cy.get('#mat-input-5').click().type('---');
        cy.get('#mat-input-5').invoke('val').then((val) => {
            const regex = new RegExp("^\\d{1,5}(\\.\\d{0,2})?$");
            expect(regex.test(val)).to.be.false;
        });
        cy.get('.justify-content-between > .button-full').click();
        cy.get('.error').should('be.visible');
    })
})