describe('Cellphone', () => {
    it('CellphoneDoesntRespectRegex', () => {

        cy.visit(`/register`)
        cy.get('#mat-input-0').click().type('Lennon');
        cy.get('#mat-input-1').click().type('John');
        cy.get('#mat-input-2').click().type('testcypress@gmail.com');
        cy.get('#mat-input-3').click().type('bonsoir');
        cy.get('#mat-input-3').invoke('val').then((val) => {
            const regex = new RegExp("^(?:(\\+33|0)[\\s-]?([1-9]{1}(?:[\\s-]?\\d){8}))$");
            expect(regex.test(val)).to.be.false;
        });
        cy.get('#mat-input-4').click().type('JohnDoe37$');
        cy.get('#mat-input-5').click().type('JohnDoe37$');
        cy.get('.float-end > .p-2').click();
        cy.get('.error').should('be.visible').should('contain', 'Un ou des champs sont invalide(s).');
    })
})