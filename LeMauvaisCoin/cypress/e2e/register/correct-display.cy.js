describe('RegisterCorrectDisplay', () => {
    it('RegisterCorrectDisplay', () => {
        cy.visit(`/register`)
        cy.get('#mat-input-0').should('be.visible');
        cy.get('#mat-input-1').should('be.visible');
        cy.get('#mat-input-2').should('be.visible');
        cy.get('#mat-input-3').should('be.visible');
        cy.get('#mat-input-4').should('be.visible');
        cy.get('#mat-input-5').should('be.visible');
        cy.get('.float-end > .p-2').should('be.visible');
    })
})