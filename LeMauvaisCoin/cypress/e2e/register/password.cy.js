describe('Password', () => {
    it('PasswordsDontMatch', () => {
        cy.visit(`/register`)
        cy.get('#mat-input-0').click().type('Lennon');
        cy.get('#mat-input-1').click().type('John');
        cy.get('#mat-input-2').click().type('johndoe@gmail.com');
        cy.get('#mat-input-3').click().type('0685848484');
        cy.get('#mat-input-4').click().type('JohnDoe37$');
        cy.get('#mat-input-5').click().type('JohnDoe01$');
        cy.get('.float-end > .p-2').click();
        cy.get('.ng-submitted > :nth-child(8)').should('contain', 'Les mots de passes saisis ne sont pas identiques.');
    })
    it('PasswordDoesntRespectRegex', () => {
        cy.visit(`/register`)
        cy.get('#mat-input-0').click().type('Lennon');
        cy.get('#mat-input-1').click().type('John');
        cy.get('#mat-input-2').click().type('testcypress@gmail.com');
        cy.get('#mat-input-3').click().type('0685848484');
        cy.get('#mat-input-4').click().type('BonsoirParis');
        cy.get('#mat-input-4').invoke('val').then((val) => {
            const regex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{10,}$");
            expect(regex.test(val)).to.be.false;
        });
        cy.get('#mat-input-5').click().type('BonsoirParis');
        cy.get('.float-end > .p-2').click();
        cy.get('.error').should('be.visible').should('contain', 'Un ou des champs sont invalide(s).');
    })
    it('PasswordTooLong', () => {
        cy.visit(`/register`)
        cy.get('#mat-input-0').click().type('Lennon');
        cy.get('#mat-input-1').click().type('John');
        cy.get('#mat-input-2').click().type('testcypress@gmail.com');
        cy.get('#mat-input-3').click().type('0685848484');
        cy.get('#mat-input-4').click().type('Ba7$'.repeat(64) + 'z');
        cy.get('#mat-input-5').click().type('Ba7$'.repeat(64) + 'z');
        cy.get('.float-end > .p-2').click();
        cy.get('.error').should('be.visible').should('contain', 'Un ou des champs sont invalide(s).');
    })
})