describe('Name', () => {
    it('NameDoesntRespectRegex', () => {
        cy.visit(`/register`)
        cy.get('#mat-input-0').click().type('JohnDoe37$');
        cy.get('#mat-input-0').invoke('val').then((val) => {
            const regex = new RegExp("^[a-zA-Z0-9]{1,30}$");
            expect(regex.test(val)).to.be.false;
        });
        cy.get('#mat-input-1').click().type('Doe');
        cy.get('#mat-input-2').click().type('testcypress@gmail.com');
        cy.get('#mat-input-3').click().type('0685848484');
        cy.get('#mat-input-4').click().type('JohnDoe37$');
        cy.get('#mat-input-5').click().type('JohnDoe37$');

        cy.get('.float-end > .p-2').click();
        cy.get('.error').should('be.visible').should('contain', 'Un ou des champs sont invalide(s).');
    });
});