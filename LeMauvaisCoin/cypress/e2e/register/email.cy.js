describe('Email', () => {
    it('EmailDoesntRespectRegex', () => {
        cy.visit(`/register`)
        cy.get('#mat-input-0').click().type('Lennon');
        cy.get('#mat-input-1').click().type('John');
        cy.get('#mat-input-2').click().type('bonsoir.jesuisMickey');
        cy.get('#mat-input-2').invoke('val').then((val) => {
            const regex = new RegExp("^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$");
            expect(regex.test(val)).to.be.false;
        });
        cy.get('#mat-input-3').click().type('0685848484');
        cy.get('#mat-input-4').click().type('JohnDoe37$');
        cy.get('#mat-input-5').click().type('JohnDoe37$');

        cy.get('.float-end > .p-2').click();
        cy.get('.error').should('be.visible').should('contain', 'Un ou des champs sont invalide(s).');
    })
    it('EmailTooLong', () => {
        cy.visit(`/register`)
        cy.get('#mat-input-0').click().type('Lennon');
        cy.get('#mat-input-1').click().type('John');
        cy.get('#mat-input-2').click().type('b'.repeat(247) + '@gmail.com');
        cy.get('#mat-input-3').click().type('0685848484');
        cy.get('#mat-input-4').click().type('JohnDoe37$');
        cy.get('#mat-input-5').click().type('JohnDoe37$');

        cy.get('.float-end > .p-2').click();
        cy.get('.error').should('be.visible').should('contain', 'Un ou des champs sont invalide(s).');
    })
})