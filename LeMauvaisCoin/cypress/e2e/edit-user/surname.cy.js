import { register } from '../../setup.js';

describe('Surname', () => {
    it('SurnameDoesntRespectRegex', () => {
        register();

        cy.get('[routerlink="/account"]').click();
        cy.get('.button-full').click();

        cy.get('#mat-input-3').click().type('Commentçamonreuf?');
        cy.get('#mat-input-3').invoke('val').then((val) => {
            const regex = new RegExp("^[a-zA-Z0-9]{1,30}$");
            expect(regex.test(val)).to.be.false;
        });

        cy.get('.mat-mdc-dialog-content > .justify-content-between > .button-full').dblclick();
        cy.get('.error').should('be.visible').should('contain', 'Un ou des champs sont invalide(s).');
    });
})