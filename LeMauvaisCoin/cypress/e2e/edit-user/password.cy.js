import { register } from '../../setup.js';

describe('Password', () => {
    it('PasswordsDontMatch', () => {
        register();
        cy.get('[routerlink="/account"]').click();
        cy.get('.button-full').click();

        cy.get('#mat-input-6').click().clear().type('BonsoirParis75*');
        cy.get('#mat-input-7').click().clear().type('BonsoirParis78*');

        cy.get('.mat-mdc-dialog-content > .justify-content-between > .button-full').dblclick();
        cy.get('.justify-content-center > :nth-child(8)').should('contain', 'Les mots de passes saisis ne sont pas identiques.');
    })
    it('PasswordDoesntRespectRegex', () => {
        register();
        cy.get('[routerlink="/account"]').click();
        cy.get('.button-full').click();

        cy.get('#mat-input-6').click().clear().type('BonsoirParis');
        cy.get('#mat-input-6').invoke('val').then((val) => {
            const regex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{10,}$");
            expect(regex.test(val)).to.be.false;
        });
        cy.get('#mat-input-7').click().clear().type('BonsoirParis');

        cy.get('.mat-mdc-dialog-content > .justify-content-between > .button-full').dblclick();
        cy.get('.error').should('be.visible').should('contain', 'Un ou des champs sont invalide(s).');
    })
    it('PasswordTooLong', () => {
        register();
        cy.get('[routerlink="/account"]').click();
        cy.get('.button-full').click();

        cy.get('#mat-input-6').click().clear().type('Ba7$'.repeat(64) + 'z');
        cy.get('#mat-input-7').click().clear().type('Ba7$'.repeat(64) + 'z');

        cy.get('.mat-mdc-dialog-content > .justify-content-between > .button-full').dblclick();
        cy.get('.error').should('be.visible').should('contain', 'Un ou des champs sont invalide(s).');
    })
})