import { register } from '../../setup.js';

describe('EmailDoesntRespectRegex', () => {
    it('EmailDoesntRespectRegex', () => {
        register();
        cy.get('[routerlink="/account"]').click();
        cy.get('.button-full').click();

        cy.get('#mat-input-4').click().clear().type('bonsoir.jesuisMickey');
        cy.get('#mat-input-4').invoke('val').then((val) => {
            const regex = new RegExp("^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$");
            expect(regex.test(val)).to.be.false;
        });

        cy.get('.mat-mdc-dialog-content > .justify-content-between > .button-full').dblclick();
        cy.get('.error').should('be.visible').should('contain', 'Un ou des champs sont invalide(s).');
    })
    it('EmailTooLong', () => {
        register();
        cy.get('[routerlink="/account"]').click();
        cy.get('.button-full').click();

        cy.get('#mat-input-4').click().clear().type('b'.repeat(247) + '@gmail.com');

        cy.get('.mat-mdc-dialog-content > .justify-content-between > .button-full').dblclick();
        cy.get('.justify-content-center > .error').should('have.text', ' Un ou des champs sont invalide(s). ')
    })
})