import { register } from '../../setup.js';

describe('CellphoneDoesntRespectRegex', () => {
    it('CellphoneDoesntRespectRegex', () => {
        register();

        cy.get('[routerlink="/account"]').click();
        cy.get('.button-full').click();

        cy.get('#mat-input-5').click().clear().type('bonsoir');
        cy.get('#mat-input-5').invoke('val').then((val) => {
            const regex = new RegExp("^(?:(\\+33|0)[\\s-]?([1-9]{1}(?:[\\s-]?\\d){8}))$");
            expect(regex.test(val)).to.be.false;
        });

        cy.get('.mat-mdc-dialog-content > .justify-content-between > .button-full').dblclick();
        cy.get('.error').should('be.visible').should('contain', 'Un ou des champs sont invalide(s).');
    })
})