import { register } from '../../setup.js';

describe('ConfirmPasswordDoesntRespectRegex', () => {
    it('ConfirmPasswordDoesntRespectRegex', () => {
        register();

        cy.get('[routerlink="/account"]').click();
        cy.get('.button-full').click();

        cy.get('#mat-input-6').click().clear().type('bonsoir');
        cy.get('#mat-input-7').click().clear().type('bonsoir');
        cy.get('#mat-input-7').invoke('val').then((val) => {
            const regex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})");
            expect(regex.test(val)).to.be.false;
        });

        cy.get('.mat-mdc-dialog-content > .justify-content-between > .button-full').dblclick();
        cy.get('.error').should('be.visible').should('contain', 'Un ou des champs sont invalide(s).');
    })
})