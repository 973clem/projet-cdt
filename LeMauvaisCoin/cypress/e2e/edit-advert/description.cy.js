import { register,removeAdvertisement,createAdvertisement } from '../../setup.js';

describe('Description', () => {
    it('DescriptionDoesntRespectRegex', () => {
        register();
        createAdvertisement();
        cy.get('#mat-tab-label-0-1').click();
        cy.get('.mat-mdc-card-actions > .button-border').first().click();

        cy.wait(500);

        cy.get('#mat-input-6').click().clear().type('Test');
        cy.get('#mat-input-7').click().clear().type('https://i.ibb.co/dmQrgXS/biko.jpg');
        cy.get('#mat-input-8').click().clear().type("b");
        cy.get('#mat-input-8').invoke('val').then((val) => {
            const regex = new RegExp("^.{10,256}$");
            expect(regex.test(val)).to.be.false;
        });
        cy.get('#mat-input-9').click().clear().type('100')

        cy.get('.justify-content-between > .button-full').click();
        cy.get('.error').should('be.visible');
        cy.get('.justify-content-between > .button-border').click();
        removeAdvertisement();
    })
})