import { register,removeAdvertisement,createAdvertisement } from '../../setup.js';

describe('Price', () => {
    it('PriceDoesntRespectRegex', () => {
        register();
        createAdvertisement();
        cy.get('#mat-tab-label-0-1').click();
        cy.get('.mat-mdc-card-actions > .button-border').first().click();

        cy.get('#mat-input-6').click().clear().type('Test');
        cy.get('#mat-input-7').click().clear().type('https://i.ibb.co/dmQrgXS/biko.jpg');
        cy.get('#mat-input-8').click().clear().type("Testing this ad, don't mind me");
        cy.get('#mat-input-9').click().clear().type('7777777');
        cy.get('#mat-input-9').invoke('val').then((val) => {
            const regex = new RegExp("^\\d{1,5}(\\.\\d{0,2})?$");
            expect(regex.test(val)).to.be.false;
        });
        cy.get('.justify-content-between > .button-full').click();
        cy.get('.error').should('be.visible');
        cy.get('.justify-content-between > .button-border').click();
        removeAdvertisement();
    })    
})