import { register,removeAdvertisement,createAdvertisement } from '../../setup.js';

describe('Image', () => {
    it('ImageDoesntRespectRegex', () => {
        register();

        createAdvertisement();
        cy.get('#mat-tab-label-0-1').click();
        cy.get('.mat-mdc-card-actions > .button-border').first().click();

        cy.get('#mat-input-6').click().clear().type('Test');
        cy.get('#mat-input-7').click().clear().type('bonsoir');
        cy.get('#mat-input-7').invoke('val').then((val) => {
            const regex = new RegExp("^(https?://\\S{0,256})$");
            expect(regex.test(val)).to.be.false;
        });
        cy.get('#mat-input-8').click().clear().type("Testing this ad, don't mind me");
        cy.get('#mat-input-9').click().clear().type('100');
        cy.get('.justify-content-between > .button-full').click();
        cy.get('.error').should('be.visible');
        cy.get('.justify-content-between > .button-border').click();
        removeAdvertisement();    
    })
})