import { register } from '../../setup.js';

describe('ButtonVisibility', () => {
    it('NotConnectedCase', () => {
        cy.visit(`/home`);
        cy.get('[routerlink="/login"]').should('be.visible');
        cy.get('[routerlink="/register"]').should('be.visible');
    })
    it('ConnectedCase', () => {
        register();
        cy.get('[routerlink="/account"]').should('be.visible');
        cy.get('[routerlink="/home"]').should('be.visible');
        cy.get('#mat-tab-label-0-1 > .mdc-tab__content > .mdc-tab__text-label').click();
        cy.get('.button-full').should('be.visible');
        cy.get('#mat-tab-label-0-2 > .mdc-tab__content > .mdc-tab__text-label').click();
        cy.get('.mat-mdc-tab-body-content > .ng-star-inserted > h1').should('be.visible');
        cy.get('#mat-tab-label-0-0 > .mdc-tab__content > .mdc-tab__text-label').click();
        cy.get('jhi-all-advertisement.ng-star-inserted > h1.p-2').should('be.visible');
        cy.get('[routerlink="/home"]').click();
    })
})

describe('ButtonRedirect', () => {
    it('LoginRedirect', () => {
        cy.visit(`/home`);
        cy.get('[routerlink="/login"]').click();
        cy.url().should('include', '/login');
    })
    it('RegisterRedirect', () => {
        cy.visit(`/home`);
        cy.get('[routerlink="/register"]').click();
        cy.url().should('include', '/register');
    })
    it('AccountRedirect', () => {
        register();
        cy.get('[routerlink="/account"]').click();
        cy.url().should('include', '/account');
    })
    it('DisconnectRedirect', () => {
        register();
        cy.get('[routerlink="/home"]').click();
        cy.url().should('include', '/home');
        cy.get('[routerlink="/login"]').should('be.visible');
        cy.get('[routerlink="/register"]').should('be.visible');
    })
    it('CreateAdvertisementRedirect', () => {
        register();
        cy.get('#mat-tab-label-0-1').click();
        cy.get('.button-full').click();
        cy.get('#mat-input-2').should('be.visible');
        cy.get('#mat-input-3').should('be.visible');
        cy.get('#mat-input-4').should('be.visible');
        cy.get('#mat-input-5').should('be.visible');
        cy.get('.justify-content-between > .button-full').should('be.visible');
    })
    it('MyProposalsRedirect', () => {
        register();
        cy.get('#mat-tab-label-0-2').click();
        cy.get('.mat-mdc-tab-body-content > .ng-star-inserted > h1').should('be.visible');
    })
    it('HomeRedirect', () => {
        register();
        cy.get('[routerlink="/account"]').click();
        cy.get('.d-inline-block').click();
        cy.url().should('include', '/home');
    })
})