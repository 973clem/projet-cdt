describe('LoginCorrectDisplay', () => {
    it('LogInCorrectDisplay', () => {
        cy.visit(`/login`)
        cy.get('#mat-input-0').should('be.visible');
        cy.get('#mat-input-1').should('be.visible');
        cy.get('.float-end > .p-2').should('be.visible');
    })
})