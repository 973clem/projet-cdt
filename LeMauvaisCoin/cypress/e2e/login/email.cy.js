describe('EmailAddress', () => {
    it('EmailDoesntRespectRegex', () => {
        cy.visit(`/login`)
        cy.get('#mat-input-0').click().type('bonsoir.jesuisMickey');
        cy.get('#mat-input-0').invoke('val').then((val) => {
            const regex = new RegExp("^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$");
            expect(regex.test(val)).to.be.false;
        });
        cy.get('#mat-input-1').click().type('JohnDoe37$');

        cy.get('.float-end > .p-2').click();
        cy.get('.error').should('be.visible').should('contain', 'Email ou mot de passe invalide.');
    })
    it('EmailTooLong', () => {
        cy.visit(`/login`)
        cy.get('#mat-input-0').click().type('b'.repeat(247) + '@gmail.com');
        cy.get('#mat-input-1').click().type('JohnDoe37$');

        cy.get('.float-end > .p-2').click();
        cy.get('.error').should('be.visible').should('contain', 'Email ou mot de passe invalide.');
    })
})