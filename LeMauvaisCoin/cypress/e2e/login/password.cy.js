describe('Password', () => {
    it('PasswordDoesntRespectRegex', () => {
        cy.visit(`/login`)
        cy.get('#mat-input-0').click().type('johndoe@gmail.com');
        cy.get('#mat-input-1').click().type('BonsoirParis');
        cy.get('#mat-input-1').invoke('val').then((val) => {
            const regex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{10,}$");
            expect(regex.test(val)).to.be.false;
        });
        cy.get('.float-end > .p-2').click();
        cy.get('.error').should('be.visible').should('contain', 'Email ou mot de passe invalide.');
    })
    it('PasswordTooLong', () => {
        cy.visit(`/login`)
        cy.get('#mat-input-0').click().type('johndoe@gmail.com');
        cy.get('#mat-input-1').click().type('Ba7$'.repeat(64) + 'z');
        cy.get('.float-end > .p-2').click();
        cy.get('.error').should('be.visible').should('contain', 'Email ou mot de passe invalide.');
    })
})