import { register } from '../../setup.js';

describe('AccountCorrectDisplay', () => {
    it('AccountCorrectDisplay', () => {
        register();

        cy.get('[routerlink="/account"]').click();
        cy.get(':nth-child(1) > .cdk-column-value').should('contain', 'Test');
        cy.get(':nth-child(2) > .cdk-column-value').should('contain', 'Cypress');
        cy.get(':nth-child(3) > .cdk-column-value').should('contain', 'testcypress@gmail.com');
        cy.get(':nth-child(4) > .cdk-column-value').should('contain', '0685848484');
        cy.get('.button-full').should('be.visible');
        cy.get('.error').should('be.visible');

        cy.get('.error').click();

        cy.wait(1000);
    })
})

describe('GoOnPageBeingDisconnected', () => {
    it('GoOnPageBeingDisconnected', () => {
        cy.visit(`/account`)
        cy.url().should('include', '/home');
    })
})