import { register } from '../../setup.js';

describe('ClickEdit', () => {
    it('ClickEdit', () => {
        register();

        cy.get('[routerlink="/account"]').click();
        cy.get('.button-full').click();
        cy.get('#mat-input-2').should('be.visible');
        cy.get('#mat-input-3').should('be.visible');
        cy.get('#mat-input-4').should('be.visible');
        cy.get('#mat-input-5').should('be.visible');
        cy.get('#mat-input-6').should('be.visible');
    
        cy.get('#mat-input-7').should('be.visible');
        cy.get('.mat-mdc-dialog-content > .justify-content-between > .button-full').should('be.visible');
        cy.get('.justify-content-between > .button-border').should('be.visible');
    })
})