export const email = 'testcypress@gmail.com'
export const password = 'JohnDoe37$'

export function register() {
    cy.visit('/login');
    cy.wait(500);
    cy.get('#mat-input-0').click().type(email);
    cy.get('#mat-input-1').click().type(password);
    cy.get('.float-end > .p-2').click();

    cy.wait(500);

    cy.url().then(url => {
        if (url.includes('login')) {
            cy.visit('/register');
            cy.get('#mat-input-0').click().type('Test');
            cy.get('#mat-input-1').click().type('Cypress');
            cy.get('#mat-input-2').click().type(email);
            cy.get('#mat-input-3').click().type('0685848484');
            cy.get('#mat-input-4').click().type(password);
            cy.get('#mat-input-5').click().type(password);
            cy.get('.float-end > .p-2').click();
        }
    })
}

export function createAdvertisement() {
    cy.get('#mat-tab-label-0-1').click();
    cy.get('.button-full').click();
    cy.get('#mat-input-2').click().type('Test');
    cy.get('#mat-input-3').click().type('https://i.ibb.co/dmQrgXS/biko.jpg');
    cy.get('#mat-input-4').click().type("Testing this ad, don't mind me");
    cy.get('#mat-input-5').click().type('100');
    cy.get('.justify-content-between > .button-full').click();
}

export function removeAdvertisement() {
    cy.get(':nth-child(1) > .mat-mdc-card > .mat-mdc-card-actions > .delete').dblclick({ force: true });
    cy.wait(1000);
}

export function removeUser() {
    cy.visit('/login');
    cy.wait(500);
    cy.get('#mat-input-0').click().type(email);
    cy.get('#mat-input-1').click().type(password);
    cy.get('.float-end > .p-2').click();

    cy.wait(500);

    cy.url().then(url => {
        if (url.includes('login')) {
            return;
        }
        cy.get('[routerlink="/account"]').click();
        cy.get('.error').dblclick({ force: true });
        cy.wait(2000);
    })
}