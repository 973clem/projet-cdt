import { PartialWithFieldValue, QueryDocumentSnapshot } from '@angular/fire/firestore';

export interface User {
  id : string,
  name: string;
  surname: string;
  email: string;
  telephone: string;
  password: string;
};

export interface Advertisement {
  id : string,
  name: string;
  price: number;
  description?: string;
  link?: string;
  user: User;
  offers: Offer[];
};

export interface Offer {
  id : string,
  price: number;
  state: string;
  user: User;
};

export const converter = <T>() => ({
  toFirestore: (data: PartialWithFieldValue<T>) => data,
  fromFirestore: (snap: QueryDocumentSnapshot) => snap.data() as T
});
