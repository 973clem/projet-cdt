import {CanActivate, Router} from "@angular/router";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root',
})
export class NotSignedInGuard implements CanActivate {
  constructor(private _router: Router) {}

  canActivate(): boolean {
    const isSignedIn = sessionStorage.getItem('id') === null;

    if (isSignedIn !== true) {
      this._router.navigate(["/"]);
    }

    return isSignedIn;
  }

}
