import {Component, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {GestionUserService} from "../../services/gestion-user.service";
import {GestionAdvertisementService} from "../../services/gestion-advertisement.service";
import {Advertisement, User} from "../../modelData/modelData";
import {zip} from "rxjs";
import {OfferModalComponent} from "../offer-modal/offer-modal.component";
import {ACCEPTEE} from "../../modelData/constOfferStates";

/**
 * Component to display all the advertisements
 */
@Component({
  selector: 'jhi-all-advertisement',
  templateUrl: './all-advertisement.component.html',
  styleUrls: ['./all-advertisement.component.scss']
})
export class AllAdvertisementComponent implements OnInit {

  advertisements : Advertisement[] = [];

  user : User | undefined;

  constructor(private dialog: MatDialog, private gestionUserService: GestionUserService,
              private gestionAdvertisementService: GestionAdvertisementService) { }

  /**
   * Get the current user and load all advertisements
   */
  ngOnInit(): void {
    zip(
      this.gestionUserService.$user,
      this.gestionAdvertisementService.getAllAdvertisement()
    ).subscribe(([user, advertisements]) => {
      this.user = user;
      this.advertisements = advertisements;
    });
  }

  /**
   * Open the pop-up to make an offer on an advertisement
   * @param advertisement
   */
  openOfferModal(advertisement : Advertisement): void {
    const dialogRef = this.dialog.open(OfferModalComponent, {
      width: '400px',
      data: advertisement,
    });

    dialogRef.afterClosed().subscribe((result) => {
    });
  }

  /**
   * Get if the selected advertisement contained an accepted offer
   * @param advertisement
   */
  getIsSold(advertisement : Advertisement){
    return advertisement.offers.some(offer => offer.state === ACCEPTEE);
  }

}
