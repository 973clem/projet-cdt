import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Advertisement, Offer} from "../../modelData/modelData";
import {User} from "../../modelData/modelData";
import {GestionAdvertisementService} from "../../services/gestion-advertisement.service";
import {GestionUserService} from "../../services/gestion-user.service";
import {GestionOfferService} from "../../services/gestion-offer.service";
import {EN_ATTENTE} from "../../modelData/constOfferStates";

/**
 * Component to make an offer on an advertisement
 */
@Component({
  selector: 'jhi-offer-modal',
  templateUrl: './offer-modal.component.html',
  styleUrls: ['./offer-modal.component.scss']
})
export class OfferModalComponent implements OnInit {

  pricePattern = '^\\d{1,5}(\\.\\d{0,2})?$';

  offerError = false;
  duplicateOfferError = false;

  user : User | undefined;
  advertisement!: Advertisement;
  bestOffer: Offer | undefined;

  offerForm = this.formBuilder.group({
    price: ['', [Validators.required, Validators.pattern(this.pricePattern)]]
  });

  constructor(private formBuilder: FormBuilder, private gestionAdvertisementService : GestionAdvertisementService, private gestionOfferService: GestionOfferService,
              private dialogRef: MatDialogRef<OfferModalComponent>, private gestionUserService : GestionUserService,
              @Inject(MAT_DIALOG_DATA) public data: Advertisement) { }

  /**
   * Get the advertisement, the best offer made on it et the current user
   */
  ngOnInit(): void {
    this.advertisement = this.data;
    this.bestOffer = this.gestionAdvertisementService.getBestOffer(this.advertisement);
    this.gestionUserService.$user.subscribe(u => {
      this.user = u;
    })
  }

  /**
   * Check if the form is valid and there is no other offer already made by the user on this advertisement
   * The create a new offer
   */
  onOkClick(): void {
    this.duplicateOfferError = false;
    if (this.offerForm.valid) {
      // test la non duplication d'une offre
      this.advertisement.offers.forEach(offer => {
        if(offer.user.id === this.user!.id){
          this.duplicateOfferError ||= true;
        }
      });
      if(!this.duplicateOfferError){
        this.gestionOfferService.createOffer({
          id: "",
          price: this.price.value,
          state: EN_ATTENTE,
          user: this.user!
        }, this.advertisement);
        this.dialogRef.close();
      }
    } else {
      this.offerError = true;
    }
  }

  /**
   * Close the pop-up
   */
  onCancelClick(): void {
    this.dialogRef.close();
  }

  /**
   * Get the price from the form
   */
  get price(): FormControl {
    return this.offerForm.get('price') as FormControl;
  }

}
