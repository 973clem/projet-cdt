import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Advertisement} from "../../modelData/modelData";
import {User} from "../../modelData/modelData";
import {GestionAdvertisementService} from "../../services/gestion-advertisement.service";
import {GestionUserService} from "../../services/gestion-user.service";

/**
 * Component to create an advertisement
 */
@Component({
  selector: 'jhi-advertisement-modal',
  templateUrl: './advertisement-modal.component.html',
  styleUrls: ['./advertisement-modal.component.scss']
})
export class AdvertisementModalComponent implements OnInit {

  namePattern = '^[a-zA-Z0-9 ]{3,30}$';
  linkPattern = '^(https?://\\S+)$';
  descriptionPattern = '^.{10,256}$';
  pricePattern = '^\\d{1,5}(\\.\\d{0,2})?$';

  adverError = false;

  user : User | undefined;

  advertisementForm = this.formBuilder.group({
    name: ['', [Validators.required, Validators.pattern(this.namePattern)]],
    link: ['', [Validators.pattern(this.linkPattern)]],
    description: ['', [Validators.required, Validators.pattern(this.descriptionPattern)]],
    price: ['', [Validators.required, Validators.pattern(this.pricePattern)]]});

  constructor(private formBuilder: FormBuilder, private gestionAdvertisementService : GestionAdvertisementService,
              private dialogRef: MatDialogRef<AdvertisementModalComponent>, private gestionUserService : GestionUserService,
              @Inject(MAT_DIALOG_DATA) public data: Advertisement) { }

  /**
   * Patch the form's values from the data if data is defined and get the current user
   */
  ngOnInit(): void {
    if (this.data) {
      this.name.patchValue(this.data.name);
      this.price.patchValue(this.data.price);
      this.link.patchValue(this.data.link);
      this.description.patchValue(this.data.description);
    }
    this.gestionUserService.$user.subscribe(u => {
      this.user = u;
    })
  }

  /**
   * Create or update the advertisement if the form is valid
   */
  onOkClick(): void {
    if (this.advertisementForm.valid) {
      // if data is defined then update
      if (this.data) {
        this.gestionAdvertisementService.updateAdvertisement({
          id: this.data.id,
          name: this.name.value,
          link: this.link.value,
          description: this.description.value,
          price: this.price.value,
          user: this.data.user,
          offers: this.data.offers
        }).subscribe(data => {
          this.dialogRef.close();
        });
        // create a new advertisement
      } else {
        this.gestionAdvertisementService.createAdvertisement({
          id: "",
          name: this.name.value,
          link: this.link.value,
          description: this.description.value,
          price: this.price.value,
          user: this.user ? this.user : {} as User,
          offers: []
        }).subscribe(data => {
          this.dialogRef.close();
        });

      }
    } else {
      this.adverError = true;
    }

  }

  /**
   * Close the pop-up
   */
  onCancelClick(): void {
    this.dialogRef.close();
  }

  /**
   * Get the name from the form
   */
  get name(): FormControl {
    return this.advertisementForm.get('name') as FormControl;
  }

  /**
   * Get the link from the form
   */
  get link(): FormControl {
    return this.advertisementForm.get('link') as FormControl;
  }

  /**
   * Get the description from the form
   */
  get description(): FormControl {
    return this.advertisementForm.get('description') as FormControl;
  }

  /**
   * Get the price from the form
   */
  get price(): FormControl {
    return this.advertisementForm.get('price') as FormControl;
  }

}
