import {Component} from '@angular/core';
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {GestionUserService} from "../../services/gestion-user.service";
import {Router} from "@angular/router";

/**
 * Component that contains form to connect the a user
 */
@Component({
  selector: 'jhi-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  emailPattern = "^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$";
  loginError = false;
  hide = true;

  loginForm = this.formBuilder.group({
    email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
    password: ['', [Validators.required]],
  });

  constructor(private formBuilder: FormBuilder, private gestionUserService : GestionUserService,
              private router: Router) { }


  /**
   * Function to connect the user using values passed in form.
   * It first checks if the association of email and password exist, if it exists connect the user.
   */
  public connection(): void {
    this.loginError = false;
    this.gestionUserService.getUserWithlogins(this.email.value, this.password.value).subscribe(data => {
      if (data.length > 0) {
        this.gestionUserService.setUser(data[0]);
        this.redirect();
        this.loginForm.reset();
      } else {
        this.loginError = true;
        this.loginForm.reset();
      }
    }, (error => {
      this.loginError = true;
      this.loginForm.reset();
    }));
  }

  /**
   * Get FormControl of password field
   */
  get password() {
    return this.loginForm?.get('password') as FormControl;
  }

  /**
   * Get email of password field
   */
  get email() {
    return this.loginForm?.get('email') as FormControl;
  }

  /**
   * Navigate to home page.
   */
  private redirect() {
    this.router.navigate(['home']);
  }

}
