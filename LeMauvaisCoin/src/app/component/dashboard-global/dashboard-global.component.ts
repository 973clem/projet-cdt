import { Component } from '@angular/core';

/**
 * Component for the dashboard page which display the component to all the advertisements
 * when the user is not connected
 */
@Component({
  selector: 'jhi-dashboard-global',
  templateUrl: './dashboard-global.component.html',
  styleUrls: ['./dashboard-global.component.scss']
})
export class DashboardGlobalComponent {

}
