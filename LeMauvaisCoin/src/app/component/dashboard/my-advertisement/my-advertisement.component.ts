import {Component, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {AdvertisementModalComponent} from "../../advertisement-modal/advertisement-modal.component";
import {Advertisement, User} from "../../../modelData/modelData";
import {GestionUserService} from "../../../services/gestion-user.service";
import {GestionAdvertisementService} from "../../../services/gestion-advertisement.service";
import {switchMap, tap} from "rxjs";
import {OffersModalComponent} from "../../offers-modal/offers-modal.component";

/**
 * Component to display all the advertisement created by the current user
 */
@Component({
  selector: 'jhi-my-advertisement',
  templateUrl: './my-advertisement.component.html',
  styleUrls: ['./my-advertisement.component.scss']
})
export class MyAdvertisementComponent implements OnInit {

  advertisements : Advertisement[] = [];

  user : User | undefined;

  constructor(private dialog: MatDialog, private gestionUserService: GestionUserService,
              private gestionAdvertisementService: GestionAdvertisementService) {}

  /**
   * Init the component and load advertisements
   */
  public ngOnInit() {
    this.loadAdvetisementsFromUser();
  }

  /**
   * Load advertisement from the current user
   */
  loadAdvetisementsFromUser(){
    this.gestionUserService.$user.pipe(
      switchMap(user => {
        if (user) {
          return this.gestionAdvertisementService.getAdvertisementsFromUser(user);
        } else {
          // Retourne un observable vide si l'utilisateur n'est pas défini
          return [];
        }
      }),
      tap(data => {
        this.advertisements = data;
      })
    ).subscribe();
  }

  /**
   * Open the pop-up to display all the offers created on the advertisement
   * @param advertisement
   */
  openOffersToAdvertisementModal(advertisement?: Advertisement): void {
    const dialogRef = this.dialog.open(OffersModalComponent, {
      width: '600px',
      height: '800px',
      data: advertisement,
    });
  }

  /**
   * Open the pop-up to create a new advertisement
   * @param advertisement
   */
  openAdvertisementModal(advertisement?: Advertisement): void {
    const dialogRef = this.dialog.open(AdvertisementModalComponent, {
      width: '400px',
      data: advertisement,
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.loadAdvetisementsFromUser();
    });
  }

  /**
   * Delete the advertisement and reload advertisements from the current user
   * @param advertisement
   */
  delete(advertisement: Advertisement): void {
    this.gestionAdvertisementService.deleteAdvertisement(advertisement).subscribe(data => {
      this.loadAdvetisementsFromUser();
    });

  }

}
