import { Component } from '@angular/core';
import {Advertisement, User} from "../../../modelData/modelData";
import {MatDialog} from "@angular/material/dialog";
import {GestionUserService} from "../../../services/gestion-user.service";
import {GestionAdvertisementService} from "../../../services/gestion-advertisement.service";
import {switchMap, tap} from "rxjs";
import {GestionOfferService} from "../../../services/gestion-offer.service";
import {ReadAdvertisementModalComponent} from "../../read-advertisement-modal/read-advertisement-modal.component";

/**
 * Component to display all offers created by the current user
 */
@Component({
  selector: 'jhi-my-offers',
  templateUrl: './my-offers.component.html',
  styleUrls: ['./my-offers.component.scss']
})
export class MyOffersComponent {

  offersWithAdvertisement : any[] = [];

  user : User | undefined;

  constructor(private dialog: MatDialog, private gestionUserService: GestionUserService,
              private gestionOfferService: GestionOfferService,
              private gestionAdvertisementService: GestionAdvertisementService) {}

  /**
   * Init the component and load all the offers from the current user
   */
  public ngOnInit() {
    this.loadOffersFromUser();
  }

  /**
   * Load all the offers from the current user with their linked advertisement
   */
  loadOffersFromUser(){
    this.gestionUserService.$user.pipe(
      switchMap(user => {
        if (user) {
          this.user = user;
          return this.gestionOfferService.getOffersFromUser(user);
        } else {
          // Retourne un observable vide si l'utilisateur n'est pas défini
          return [];
        }
      }),
      tap(data => {
        this.offersWithAdvertisement = [];
        // link each offer to their advertisement and the best offer made
        data.forEach(offer => {
          this.gestionAdvertisementService.getAdvertisementFromOffer(offer).subscribe(advertisement => {
            let offerWithAdvertisement = {
              id: offer.id,
              price: offer.price,
              state: offer.state,
              user: offer.user,
              advertisement: advertisement,
              bestOffer: this.gestionAdvertisementService.getBestOfferFromAnotherUser(advertisement, this.user!)
            };
            this.offersWithAdvertisement.push(offerWithAdvertisement);
          })
        }
        );
      })
    ).subscribe();
  }

  /**
   * Open the pop-up to read more on the advertisement
   * @param advertisement
   */
  openAdvertisementModal(advertisement?: Advertisement): void {
    const dialogRef = this.dialog.open(ReadAdvertisementModalComponent, {
      width: '400px',
      data: advertisement,
    });

    dialogRef.afterClosed().subscribe((result) => {
    });
  }

  /**
   * Delete the offer and reload offers
   * @param offersWithAdvertisement
   */
  delete(offersWithAdvertisement: any): void {
    this.gestionOfferService.deleteOfferWithAdvertisement(offersWithAdvertisement, offersWithAdvertisement.advertisement).subscribe(() => {
      this.loadOffersFromUser();
    });

  }

}
