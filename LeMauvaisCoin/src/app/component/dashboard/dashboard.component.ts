import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from "../../modelData/modelData";
import {GestionUserService} from "../../services/gestion-user.service";
import {MyOffersComponent} from "./my-offers/my-offers.component";
import {MyAdvertisementComponent} from "./my-advertisement/my-advertisement.component";
import {AllAdvertisementComponent} from "../all-advertisement/all-advertisement.component";

/**
 * Component to display all advertisement and a tab to access all others functionalities
 */
@Component({
  selector: 'jhi-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit{

  user : User | undefined;

  @ViewChild(MyAdvertisementComponent) myAdvertisements: MyAdvertisementComponent | undefined
  @ViewChild(AllAdvertisementComponent) allAdvertisements: AllAdvertisementComponent | undefined
  @ViewChild(MyOffersComponent) myOffers: MyOffersComponent | undefined

  constructor(private gestionUserService : GestionUserService) { }

  /**
   * Get the current user
   */
  ngOnInit(): void {
    this.gestionUserService.$user.subscribe(u => {
      this.user = u;
    })
  }

  /**
   * Reload the components on change
   * @param event
   */
  update(event : any) : void {
    this.allAdvertisements?.ngOnInit()
    this.myAdvertisements?.ngOnInit();
    this.myOffers?.ngOnInit();
  }
}
