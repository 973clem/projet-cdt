import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OffersModalComponent } from './offers-modal.component';

describe('OfferModalComponent', () => {
  let component: OffersModalComponent;
  let fixture: ComponentFixture<OffersModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OffersModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OffersModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
