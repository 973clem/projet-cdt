import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Advertisement, Offer} from "../../modelData/modelData";
import {User} from "../../modelData/modelData";
import {GestionAdvertisementService} from "../../services/gestion-advertisement.service";
import {GestionOfferService} from "../../services/gestion-offer.service";
import {ACCEPTEE, EN_ATTENTE, REFUSEE} from "../../modelData/constOfferStates";

/**
 * Component to show all offers create for an advertisement, user can refuse or accept an offer
 */
@Component({
  selector: 'jhi-offers-modal',
  templateUrl: './offers-modal.component.html',
  styleUrls: ['./offers-modal.component.scss']
})
export class OffersModalComponent implements OnInit {

  offers : any[] = [];

  user : User | undefined;

  constructor(private gestionOfferService: GestionOfferService,
              private dialogRef: MatDialogRef<OffersModalComponent>,
              private gestionAdvertisementService: GestionAdvertisementService,
              @Inject(MAT_DIALOG_DATA) public data: Advertisement) {}

  /**
   * Get all the offer from an advertisement
   */
  public ngOnInit() {
    if(this.data){
      this.offers = this.data.offers;
    }
  }

  /**
   * Load the object advertisement from the database
   */
  loadOffersFromAdvertisement(){
    this.gestionAdvertisementService.getAdvertisementWithId(this.data.id);
  }

  /**
   * change the offer's state to accept and change to refuse the others
   * @param offer
   */
  onAcceptedClick(offer : Offer): void {
    this.data.offers.forEach(o => {
      if(o.id !== offer.id){
        this.gestionOfferService.changeStateOfferWithAdvertisement(o, this.data, REFUSEE);
      }
    });
    this.gestionOfferService.changeStateOfferWithAdvertisement(offer, this.data, ACCEPTEE).subscribe(() => {
      this.loadOffersFromAdvertisement();
    });
  }

  /**
   * change the offer's state to refuse
   * @param offer
   */
  onRefusedClick(offer : Offer): void {
    this.gestionOfferService.changeStateOfferWithAdvertisement(offer, this.data, REFUSEE).subscribe( () => this.loadOffersFromAdvertisement());
  }

  /**
   * Close the pop-up
   */
  onReturnClick(): void {
    this.dialogRef.close();
  }

  protected readonly EN_ATTENTE = EN_ATTENTE;
}
