import {Component, OnInit} from '@angular/core';
import {GestionUserService} from "../../services/gestion-user.service";
import {User} from "../../modelData/modelData";
import {map, switchMap, tap} from "rxjs";
import {MatDialog} from "@angular/material/dialog";
import {UserModalComponent} from "../user-modal/user-modal.component";
import {GestionAdvertisementService} from "../../services/gestion-advertisement.service";
import {GestionOfferService} from "../../services/gestion-offer.service";
import {Router} from "@angular/router";

/**
 * Component that allows the user to see its personnal data, and to modify or delete its account.
 */
@Component({
  selector: 'jhi-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss']
})
export class MyAccountComponent implements OnInit{

  user : User | undefined;
  displayedColumns: string[] = ['id', 'value'];
  dataSource = [{
    id: '', value: ''
  }];
  userDefined = false

  constructor(private gestionUserService : GestionUserService, private dialog: MatDialog,
              private gestionAdvertisement : GestionAdvertisementService,
              private gestionOffer: GestionOfferService, private router: Router) {
  }

  /**
   * Assign values of current user into table to be displayed.
   */
  ngOnInit(): void {
    this.gestionUserService.$user.pipe(
      map(user => {
        this.user = user;
        return [
          { id: 'Nom', value: user.surname },
          { id: 'Prénom', value: user.name },
          { id: 'Adresse mail', value: user.email },
          { id: 'Numéro de téléphone', value: user.telephone }
        ];
      }),
    ).subscribe(dataSource => {
      this.dataSource = dataSource;
      this.isUserDefined();
    });
  }


  /**
   * Check if user is defined or not.
   */
  isUserDefined(): void {
    this.userDefined = this.user?.id !== undefined;
  }

  /**
   * Open user modification modal, pass user as data to the modal.
   * When the modal closes, reload the user to see the modifications on screen.
   * @param user
   */
  openUserModal(user?: User): void {
    const dialogRef = this.dialog.open(UserModalComponent, {
      width: '450px',
      data: user,
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.loadUser();
    });
  }

  /**
   * Reload the user and all of his information.
   * @private
   */
  private loadUser(){
    this.gestionUserService.$user.pipe(
      switchMap(user => {
        if (user) {
          return this.gestionUserService.$user;
        } else {
          // Retourne un observable vide si l'utilisateur n'est pas défini
          return [];
        }
      }),
      tap(data => {
        this.user = data;
      })
    ).subscribe();
  }

  /**
   * Call the service delete function and disconnect the user.
   * @param user
   */
  deleteUser(user?: User) {
    this.gestionUserService.deleteUser(user? user : {} as User, this.gestionAdvertisement, this.gestionOffer)
    this.disconnect()
  }

  /**
   * Disconnect the user, clears the session data and redirect to home page.
   */
  disconnect(): void {
    this.gestionUserService.setUser({} as User);
    sessionStorage.clear()
    this.redirect();
  }

  /**
   * Redirect to home page.
   * @private
   */
  private redirect() {
    this.router.navigate(['home']);
  }

}
