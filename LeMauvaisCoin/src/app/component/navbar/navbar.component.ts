import {Component, OnInit} from '@angular/core';
import {User} from "../../modelData/modelData";
import {GestionUserService} from "../../services/gestion-user.service";
import {GestionAdvertisementService} from "../../services/gestion-advertisement.service";
import {Router} from "@angular/router";

/**
 * Navbar component to display login or my account buttons and create an account our log out buttons.
 */
@Component({
  selector: 'jhi-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit{

  user : User | undefined;
  userDefined = false;

  constructor(private gestionUserService : GestionUserService, private gestionAdvertisementService : GestionAdvertisementService,
              private router : Router) {

  }

  /**
   * Get the current user anc check if is a valid user
   */
  ngOnInit(): void {
    this.gestionUserService.$user.subscribe(u => {
      this.user = u;
      this.isUserDefined();
    })
  }

  /**
   * check if the user is valid and defined
   */
  isUserDefined(): void {
    this.userDefined = this.user?.id !== undefined;
  }

  /**
   * log out from the current session
   */
  disconnect(): void {
    this.gestionUserService.setUser({} as User);
    sessionStorage.clear()
    this.redirect();
  }

  /**
   * redirect to the home page
   * @private
   */
  private redirect() {
    this.router.navigate(['home']);
  }
}
