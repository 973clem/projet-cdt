import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadAdvertisementModalComponent } from './read-advertisement-modal.component';

describe('AdvertisementModalComponent', () => {
  let component: ReadAdvertisementModalComponent;
  let fixture: ComponentFixture<ReadAdvertisementModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReadAdvertisementModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReadAdvertisementModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
