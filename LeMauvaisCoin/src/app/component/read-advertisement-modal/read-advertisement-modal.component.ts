import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Advertisement} from "../../modelData/modelData";
import {User} from "../../modelData/modelData";
import {GestionAdvertisementService} from "../../services/gestion-advertisement.service";
import {GestionUserService} from "../../services/gestion-user.service";

/**
 * Component to read in a pop-up the data from an advertisement
 */
@Component({
  selector: 'jhi-read-advertisement-modal',
  templateUrl: './read-advertisement-modal.component.html',
  styleUrls: ['./read-advertisement-modal.component.scss']
})
export class ReadAdvertisementModalComponent implements OnInit {

  user : User | undefined;

  constructor(private formBuilder: FormBuilder, private gestionAdvertisementService : GestionAdvertisementService,
              private dialogRef: MatDialogRef<ReadAdvertisementModalComponent>, private gestionUserService : GestionUserService,
              @Inject(MAT_DIALOG_DATA) public advertisement: Advertisement) { }

  /**
   * Get the current user
   */
  ngOnInit(): void {
    this.gestionUserService.$user.subscribe(u => {
      this.user = u;
    })
  }

  /**
   * Close the pop-up
   */
  onCancelClick(): void {
    this.dialogRef.close();
  }

}
