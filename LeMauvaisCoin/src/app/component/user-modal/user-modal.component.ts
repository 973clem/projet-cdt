import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {GestionUserService} from "../../services/gestion-user.service";
import {map, tap} from "rxjs";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {User} from "../../modelData/modelData";
import {Router} from "@angular/router";
import {GestionAdvertisementService} from "../../services/gestion-advertisement.service";
import {GestionOfferService} from "../../services/gestion-offer.service";

/**
 * Service used for all advetisements related queries.
 */
@Component({
  selector: 'jhi-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.scss']
})
export class UserModalComponent implements OnInit{

  namePattern = "^[a-zA-Z0-9]{1,30}$";
  passwordPattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{10,256}$";
  emailPattern =  "^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$";
  phonePattern = '^(?:(\\+33|0)[\\s-]?([1-9]{1}(?:[\\s-]?\\d){8}))$';

  hide = true;
  userError = false;
  emailIsAlreadyTaken = false;
  passwordsAreNotMatching = false;
  sameUser = false;

  user : User | undefined

  userForm = this.formBuilder.group({
    name: ['', [Validators.pattern(this.namePattern)]],
    surname: ['', [Validators.pattern(this.namePattern)]],
    email: ['', [Validators.pattern(this.emailPattern), Validators.maxLength(256)]],
    password: ['', [Validators.pattern(this.passwordPattern)]],
    confirmPassword: ['', [Validators.pattern(this.passwordPattern)]],
    phone: ['', [Validators.pattern(this.phonePattern)]]
  });

  constructor(private formBuilder: FormBuilder, private dialogRef: MatDialogRef<UserModalComponent>,
              private gestionUserService : GestionUserService, @Inject(MAT_DIALOG_DATA) public data: User,
              private router: Router, private gestionAdvertisement : GestionAdvertisementService,
              private gestionOffer: GestionOfferService) { }

  ngOnInit(): void {
    if (this.data) {
      this.name.patchValue(this.data.name)
      this.surname.patchValue(this.data.surname)
      this.email.patchValue(this.data.email)
      this.phone.patchValue(this.data.telephone)
      this.password.patchValue(this.data.password)
      this.confirmPassword.patchValue(this.data.password)
    }
    this.gestionUserService.$user.subscribe(u => {
      this.user = u;
    })
  }

  /**
   * Check if the user changed any value, if not displays an error message.
   * Check if the passwords are matching, if not displays an error message.
   * If form is valid and at least one value has changes, will call the modify user function,
   * set the new user to be displayed and navigate back to account.
   */
  onOkClick(): void {

    this.sameUser =  (
        this.user?.name === this.name.value &&
        this.user?.surname === this.surname.value &&
        this.user?.email === this.email.value &&
        this.user?.password === this.password.value &&
        this.user?.telephone === this.phone.value
    )

    if (this.sameUser) { return;}

    this.passwordsAreNotMatching = this.password.value !== this.confirmPassword.value;
    if (this.passwordsAreNotMatching || !this.userForm.valid) {
      this.userError = true;
      return;
    }

    this.gestionUserService
      .getUserWithEmail(this.email.value)
      .pipe(
        map(data => {
          this.emailIsAlreadyTaken = data.length > 0 && this.user?.email !== this.email.value ;

          // Utiliser 'of' pour retourner la valeur actuelle ou rien
          return data;
        }),
        tap(data => {
          if (!this.emailIsAlreadyTaken) {
            const modifiedUser = this.gestionUserService.modifyUser({
              name: this.name.value,
              surname: this.surname.value,
              email: this.email.value,
              password: this.password.value,
              telephone: this.phone.value,
              id: this.user?.id ? this.user.id : ''
            }, this.gestionAdvertisement, this.gestionOffer);

            this.gestionUserService.setUser(modifiedUser);
            this.router.navigate(['/account']);
            this.dialogRef.close();
            this.userForm.reset();
          }
        })
      )
      .subscribe();
  }

  /**
   * Closes the modal.
   */
  onCancelClick(): void {
    this.dialogRef.close();
  }

  /**
   * Get FormControl field name.
   */
  get name(): FormControl {
    return this.userForm.get('name') as FormControl;
  }

  /**
   * Get FormControl field surname.
   */
  get surname(): FormControl {
    return this.userForm.get('surname') as FormControl;
  }

  /**
   * Get FormControl field password.
   */
  get password(): FormControl {
    return this.userForm.get('password') as FormControl;
  }

  /**
   * Get FormControl field confirmPassword.
   */
  get confirmPassword(): FormControl {
    return this.userForm.get('confirmPassword') as FormControl;
  }

  /**
   * Get FormControl field email.
   */
  get email(): FormControl {
    return this.userForm.get('email') as FormControl;
  }

  /**
   * Get FormControl field phone.
   */
  get phone(): FormControl {
    return this.userForm.get('phone') as FormControl;
  }

}
