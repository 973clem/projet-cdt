import {Component} from '@angular/core';
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {GestionUserService} from "../../services/gestion-user.service";
import {Router} from "@angular/router";
import {switchMap, tap} from "rxjs";

/**
 * Component register allows a user to create an account by filling form values.
 */
@Component({
  selector: 'jhi-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  namePattern = "^[a-zA-Z0-9]{1,30}$";
  passwordPattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{10,256}$";
  emailPattern =  "^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$";
  phonePattern = '^(?:(\\+33|0)[\\s-]?([1-9]{1}(?:[\\s-]?\\d){8}))$';

  hide = true;
  userError = false;
  emailIsAlreadyTaken = false;
  passwordsAreNotMatching = false;

  userForm = this.formBuilder.group({
    name: ['', [Validators.required, Validators.pattern(this.namePattern)]],
    surname: ['', [Validators.required, Validators.pattern(this.namePattern)]],
    email: ['', [Validators.required, Validators.pattern(this.emailPattern), Validators.maxLength(256)]],
    password: ['', [Validators.required, Validators.pattern(this.passwordPattern)]],
    confirmPassword: ['', [Validators.required, Validators.pattern(this.passwordPattern)]],
    phone: ['', [Validators.required, Validators.pattern(this.phonePattern)]]
  });

  constructor(private formBuilder: FormBuilder, private gestionUserService : GestionUserService,
              private router: Router) { }

  /**
   * Get FormControl field name.
   */
  get name(): FormControl {
    return this.userForm.get('name') as FormControl;
  }

  /**
   * Get FormControl field surname.
   */
  get surname(): FormControl {
    return this.userForm.get('surname') as FormControl;
  }

  /**
   * Get FormControl field password.
   */
  get password(): FormControl {
    return this.userForm.get('password') as FormControl;
  }

  /**
   * Get FormControl field confirmPassword.
   */
  get confirmPassword(): FormControl {
    return this.userForm.get('confirmPassword') as FormControl;
  }

  /**
   * Get FormControl field email.
   */
  get email(): FormControl {
    return this.userForm.get('email') as FormControl;
  }

  /**
   * Get FormControl field phone.
   */
  get phone(): FormControl {
    return this.userForm.get('phone') as FormControl;
  }

  /**
   * Creates a user if passwords are matching and email is not already taken,
   * connects the user and navigate to home page.
   */
  createUser(): void {
    this.passwordsAreNotMatching = this.password.value !== this.confirmPassword.value;
    if (this.passwordsAreNotMatching || !this.userForm.valid) {
      this.userError = true;
      return;
    }

    this.gestionUserService
      .getUserWithEmail(this.email.value)
      .pipe(
        switchMap(data => {
          this.emailIsAlreadyTaken = data.length > 0;
          return this.emailIsAlreadyTaken ? [] : this.gestionUserService.createUser({
            name: this.name.value,
            surname: this.surname.value,
            email: this.email.value,
            password: this.password.value,
            telephone: this.phone.value,
            id: ''
          });
        }),
        tap(user => {
          if (user) {
            this.gestionUserService.setUser(user);
            this.router.navigate(['home/dashboard']);
            this.userForm.reset();
          }
        })
      )
      .subscribe();
  }

}
