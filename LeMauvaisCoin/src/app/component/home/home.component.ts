import {Component, OnInit} from '@angular/core';
import {GestionUserService} from "../../services/gestion-user.service";
import {User} from "../../modelData/modelData";

/**
 * Component of the home page display dashboard or dashboard global if the user is connected or not
 */
@Component({
  selector: 'jhi-root',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  title = 'LeMauvaisCoin';

  user : User | undefined;
  constructor(private gestionUserService : GestionUserService) { }

  /**
   * Get the current user
   */
  ngOnInit() {
    this.gestionUserService.$user.subscribe(u => {
      this.user = u;
    })
  }
}
