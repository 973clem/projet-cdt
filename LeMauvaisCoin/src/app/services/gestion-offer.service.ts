import { Injectable } from '@angular/core';
import { collection, getDocs, getDoc, addDoc, deleteDoc, setDoc, doc, query, where } from 'firebase/firestore/lite';
import { db } from "../app.module";
import {Offer, converter, User, Advertisement} from "../modelData/modelData";
import {from, map} from "rxjs";

/**
 * Service used for all offers related queries.
 */
@Injectable({
  providedIn: 'root'
})
export class GestionOfferService {

  tableName = 'offer';

  advertisementTable = 'advertisement';
  constructor() { }

  /**
   * Gets all offers.
   */
  getAllOffer() {
    const offerCol = collection(db, this.tableName).withConverter(converter<Offer>());
    return from(getDocs(offerCol)).pipe(
      map(querySnapshot => querySnapshot.docs.map(doc => doc.data()))
    );
  }

  /**
   * Gets all offer with given user.
   * @param user
   */
  getOffersFromUser(user : User) {
    const q = query(collection(db, this.tableName), where("user.id", "==", user.id)).withConverter(converter<Offer>());
    return from(getDocs(q)).pipe(
      map(querySnapshot => querySnapshot.docs.map(doc => doc.data()))
    );
  }

  /**
   * Change states of given offer in offer and advertisement tables.
   * @param offer
   * @param advertisement
   * @param newState
   */
  changeStateOfferWithAdvertisement(offer : Offer, advertisement : Advertisement, newState : string){
    offer.state = newState;
    advertisement.offers.forEach(o => {
      if(offer.id === o.id){
        offer.state = newState;
      }
    });
    from(setDoc(doc(db, this.advertisementTable, advertisement.id), advertisement));
    return from(setDoc(doc(db, this.tableName, offer.id), offer));
  }

  /**
   * Creates a new offer.
   * @param offer
   * @param advertisement
   */
  createOffer(offer : Offer, advertisement : Advertisement) {
    from(addDoc(collection(db, this.tableName), offer)).subscribe(docRef => {
      offer.id = docRef.id;
      setDoc(doc(db, this.tableName, offer.id), offer)

      advertisement.offers.push(offer);
      setDoc(doc(db, this.advertisementTable, advertisement.id), advertisement);
      return offer;
    });
    return offer;
  }

  /**
   * Delete an offer and its reference in advertisement table.
   * @param offer
   * @param advertisement
   */
  deleteOfferWithAdvertisement(offer : Offer, advertisement : Advertisement) {
    advertisement.offers = advertisement.offers.filter(o => offer.id !== o.id);
    from(setDoc(doc(db, this.advertisementTable, advertisement.id), advertisement));

    return from(deleteDoc(doc(db, this.tableName, offer.id)));
  }

}
