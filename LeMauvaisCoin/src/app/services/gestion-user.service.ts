import { Injectable } from '@angular/core';
import { collection, getDocs, addDoc, deleteDoc, setDoc, doc, query, where } from 'firebase/firestore/lite';
import { db } from "../app.module";
import {User, converter, Advertisement, Offer} from "../modelData/modelData";
import {BehaviorSubject, from, map, Observable} from "rxjs";
import {GestionAdvertisementService} from "./gestion-advertisement.service";
import {GestionOfferService} from "./gestion-offer.service";


/**
 * Service used for all users related queries.
 */
@Injectable({
  providedIn: 'root'
})

export class GestionUserService {

  public user = new BehaviorSubject<User>({} as User);
  public $user = this.user.asObservable();

  public users = new BehaviorSubject<User[]>([]);
  public $users = this.users.asObservable();

  private tableName = 'user';

  constructor() { this.checkAndSetUserFromSession();}

  /**
   * Gets user(s) using associated to an email.
   * @param email
   */
  getUserWithEmail(email : string): Observable<User[]> {
    const q = query(collection(db, this.tableName), where("email", "==", email))
      .withConverter(converter<User>());

    return from(getDocs(q)).pipe(
      map(querySnapshot => querySnapshot.docs.map(doc => doc.data()))
    );
  }

  /**
   * Gets a user by its email and password.
   * @param email
   * @param password
   */
  getUserWithlogins(email : string, password : string) : Observable<User[]> {
    const q = query(collection(db, this.tableName), where("email", "==", email),
        where("password", "==", password)).withConverter(converter<User>());

    return from(getDocs(q)).pipe(
        map(querySnapshot => querySnapshot.docs.map(doc => doc.data()))
    );
  }

  /**
   * Creates a new user.
   * @param user
   */
  createUser(user : User) {
    return from(addDoc(collection(db, this.tableName), user)).pipe(map(docRef => {
      user.id = docRef.id;
      setDoc(doc(db, this.tableName, user.id), user);
      return user;
    }));
  }

  /**
   * Modify a user in table user and all his references in tables advertisement and offer.
   * @param user
   * @param advertisementService
   * @param offersService
   */
  modifyUser(user: User, advertisementService: GestionAdvertisementService, offersService: GestionOfferService){
    offersService.getOffersFromUser(user).subscribe( offers => {
      offers.forEach(o => {
        if (user.id === o.user.id) {
          const offer: Offer = {
            id : o.id,
            user: user,
            price: o.price,
            state: o.state
          }
          from(setDoc(doc(db, 'offer', o.id), offer))
        }
      })
      })

    advertisementService.getAdvertisementsFromUser(user).subscribe(advertisements => {
      advertisements.forEach(a => {
        if (user.id === a.user.id) {
          const advertisement: Advertisement = {
            user: user,
            id: a.id,
            price: a.price,
            name: a.name,
            offers: a.offers,
            link:a.link ? a.link : '',
            description: a.description ? a.description : ''
          }
          from(setDoc(doc(db, 'advertisement', a.id), advertisement))

        }
      })
    })

    from(setDoc(doc(db, this.tableName, user.id), user));
    return user;
  }

  /**
   * Delete user and all the offers he has made, and all the advertisements he made and offers related to his advertisements.
   * @param user
   * @param advertisementService
   * @param offersService
   */
  public deleteUser(user : User, advertisementService: GestionAdvertisementService, offersService: GestionOfferService) {
    offersService.getOffersFromUser(user).subscribe( offers => {
      offers.forEach(o => {
        if (user.id === o.user.id) {
          from(deleteDoc(doc(db, 'offer', o.id)))
        }
      })
    })

    advertisementService.getAdvertisementsFromUser(user).subscribe(advertisements => {
      advertisements.forEach(a => {
        if (user.id === a.user.id) {
          a.offers.forEach(o => {
            from(deleteDoc(doc(db, 'offer', o.id)))
          });

          from(deleteDoc(doc(db, 'advertisement', a.id)))
        }
      })
    })
    sessionStorage.clear()
    return from(deleteDoc(doc(db, this.tableName, user.id)));
  }

  /**
   * Sets user as current user.
   * @param user
   */
  setUser(user: User) {
    this.user.next(user)
    this.setSession(user)
  }

  /**
   * Store user in session.
   * @param user
   * @private
   */
  private setSession(user : User): void {
    sessionStorage.setItem("surname", user.surname)
    sessionStorage.setItem("name", user.name)
    sessionStorage.setItem("email", user.email)
    sessionStorage.setItem("id", user.id)
    sessionStorage.setItem("password", user.password)
    sessionStorage.setItem("telephone", user.telephone)
  }

  /**
   * Check if user is existing in session, if so, sets it to current user.
   * @private
   */
  private checkAndSetUserFromSession() {
    const storedUser: User = {
      id: sessionStorage.getItem('id') ?? '',
      surname: sessionStorage.getItem('surname') ?? '',
      email: sessionStorage.getItem('email') ?? '',
      name: sessionStorage.getItem('name') ?? '',
      password: sessionStorage.getItem('password') ?? '',
      telephone: sessionStorage.getItem('telephone') ?? ''
    };

    // Vérifier si au moins l'ID de l'utilisateur est présent
    if (storedUser.id) {
      this.setUser(storedUser);
    }
  }

}
