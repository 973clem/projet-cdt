import { Injectable } from '@angular/core';
import { collection, getDocs, getDoc, addDoc, deleteDoc, setDoc, doc, query, where } from 'firebase/firestore/lite';
import { db } from "../app.module";
import {Advertisement, converter, Offer, User} from "../modelData/modelData";
import {from, map, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})

export class GestionAdvertisementService {

  tableName = 'advertisement';
  constructor() { }

  /**
   * Returns all advertisements.
   */
  getAllAdvertisement() {
    const advertisementCol = collection(db, this.tableName).withConverter(converter<Advertisement>());
    return from(getDocs(advertisementCol)).pipe(
      map(querySnapshot => querySnapshot.docs.map(doc => doc.data()))
    );
  }

  /**
   * Return advertisements with given id.
   * @param id
   */
  getAdvertisementWithId(id : string){
    const docRef = doc(db, this.tableName, id).withConverter(converter<Advertisement>());
    return from(getDoc(docRef)).pipe(map(documentsnapshot => documentsnapshot.data() as Advertisement));
  }

  /**
   * Gets all the advertisements of one user.
   * @param user
   */
  getAdvertisementsFromUser(user : User) {
    const q = query(collection(db, this.tableName), where("user.id", "==", user.id)).withConverter(converter<Advertisement>());
    return from(getDocs(q)).pipe(
      map(querySnapshot => querySnapshot.docs.map(doc => doc.data()))
    );
  }

  /**
   * Gets all the advertisements related to one offer.
   * @param offer
   */
  getAdvertisementFromOffer(offer : Offer) {
    const q = query(collection(db, this.tableName)).withConverter(converter<Advertisement>());
    return from(getDocs(q)).pipe(
      map(querySnapshot => {
        let resultAdvertisement! : Advertisement;
        querySnapshot.forEach(doc => {
          const advertisement = doc.data() as Advertisement;
          if (advertisement.offers && advertisement.offers.find(o => o.id === offer.id)) {
            resultAdvertisement = advertisement;
          }
        });
        return resultAdvertisement;
      })
    );
  }

  /**
   * Update an advertisement.
   * @param advertisement
   */
  updateAdvertisement(advertisement : Advertisement){
    return from(setDoc(doc(db, this.tableName, advertisement.id), advertisement));
  }

  /**
   * Created a new advertisement.
   * @param advertisement
   */
  createAdvertisement(advertisement : Advertisement) {
    return of(from(addDoc(collection(db, this.tableName), advertisement)).subscribe(docRef => {
      advertisement.id = docRef.id;
      setDoc(doc(db, this.tableName, advertisement.id), advertisement);
    }));
  }

  /**
   * Delete an advertisement and all the offers received for it.
   * @param advertisement
   */
  deleteAdvertisement(advertisement : Advertisement) {
    advertisement.offers.forEach(offer => {
      from(deleteDoc(doc(db, 'offer', offer.id)))
    })
    return from(deleteDoc(doc(db, this.tableName, advertisement.id)));
  }

  /**
   * Get the best (higher price) offer for one advertisement.
   * @param advertisement
   */
  getBestOffer(advertisement : Advertisement){
    if (advertisement.offers.length === 0) {
      return undefined;
    }

    let bestOffer = advertisement.offers[0];
    advertisement.offers.forEach(offer => {
      if(offer.price > bestOffer.price){
        bestOffer = offer;
      }
    })
    return bestOffer;
  }

  /**
   * Gets the best (higher price) offer for one advertisement that has not been made by a specific user.
   * @param advertisement
   * @param user
   */
  getBestOfferFromAnotherUser(advertisement : Advertisement, user : User){
    if (advertisement.offers.length === 0) {
      return undefined;
    }

    let bestOffer : Offer = {
      id : "",
      price : 0,
      state : "",
      user : user
    };
    advertisement.offers.forEach(offer => {
      if(offer.price > bestOffer.price && offer.user.id !== user.id){
        bestOffer = offer;
      }
    })
    if(bestOffer.user.id === user.id){
      return undefined;
    }
    return bestOffer;
  }
}
