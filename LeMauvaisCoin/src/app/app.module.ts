import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { getFirestore } from 'firebase/firestore/lite';
import { initializeApp } from "firebase/app";

import { AppRoutingModule } from './app-routing.module';
import { GestionUserService } from './services/gestion-user.service';
import { GestionAdvertisementService } from './services/gestion-advertisement.service';
import { GestionOfferService } from './services/gestion-offer.service';

import { HomeComponent } from './component/home/home.component';
import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { MyAccountComponent } from './component/my-account/my-account.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { FooterComponent } from './component/footer/footer.component';
import {AppComponent} from "./app.component";
import {MyAdvertisementComponent} from './component/dashboard/my-advertisement/my-advertisement.component';
import {ReactiveFormsModule} from "@angular/forms";
import {NgOptimizedImage} from "@angular/common";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatCardModule} from "@angular/material/card";
import {MatIconModule} from "@angular/material/icon";
import { DashboardGlobalComponent } from './component/dashboard-global/dashboard-global.component';
import { AdvertisementModalComponent } from './component/advertisement-modal/advertisement-modal.component';
import {OfferModalComponent} from "./component/offer-modal/offer-modal.component";
import {MatDialogModule} from "@angular/material/dialog";
import {MatTabsModule} from "@angular/material/tabs";
import { AllAdvertisementComponent } from './component/all-advertisement/all-advertisement.component';
import { MyOffersComponent } from './component/dashboard/my-offers/my-offers.component';
import {MatTableModule} from "@angular/material/table";
import { UserModalComponent } from './component/user-modal/user-modal.component';
import {OffersModalComponent} from "./component/offers-modal/offers-modal.component";
import {ReadAdvertisementModalComponent} from "./component/read-advertisement-modal/read-advertisement-modal.component";
import {NotSignedInGuard} from "./modelData/notSignedInGuard";
import {SignedInGuard} from "./modelData/signedInGuard";

const firebaseConfig = {
  apiKey: "AIzaSyDc4Yv3oUJWlbbydPgyjJBacFVSvO2rQoE",
  authDomain: "lemauvaiscoin-a84ba.firebaseapp.com",
  projectId: "lemauvaiscoin-a84ba",
  storageBucket: "lemauvaiscoin-a84ba.appspot.com",
  messagingSenderId: "313934744399",
  appId: "1:313934744399:web:5f337a38f9c73b7911133a",
  measurementId: "G-ZHLK1LLBP7"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    HomeComponent,
    MyAccountComponent,
    NavbarComponent,
    FooterComponent,
    MyAdvertisementComponent,
    DashboardGlobalComponent,
    AdvertisementModalComponent,
    ReadAdvertisementModalComponent,
    OfferModalComponent,
    AllAdvertisementComponent,
    MyOffersComponent,
    UserModalComponent,
    OffersModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgOptimizedImage,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatIconModule,
    MatDialogModule,
    MatTabsModule,
    MatTableModule
  ],
  providers: [
    GestionUserService,
    GestionAdvertisementService,
    GestionOfferService,
    NotSignedInGuard,
    SignedInGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
