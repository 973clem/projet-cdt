import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./component/home/home.component";
import {LoginComponent} from "./component/login/login.component";
import {DashboardComponent} from "./component/dashboard/dashboard.component";
import {MyAccountComponent} from "./component/my-account/my-account.component";
import {RegisterComponent} from "./component/register/register.component";
import {MyAdvertisementComponent} from "./component/dashboard/my-advertisement/my-advertisement.component";
import {NotSignedInGuard} from "./modelData/notSignedInGuard";
import {SignedInGuard} from "./modelData/signedInGuard";

export const appRouteList: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [NotSignedInGuard],
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [NotSignedInGuard],
  },
  {
    path: 'nouvelleAnnonce',
    component: MyAdvertisementComponent,
    canActivate: [SignedInGuard]
  },
  {
    path: 'account',
    component: MyAccountComponent,
    canActivate: [SignedInGuard]
  },
  {
    path: 'home',
    component: HomeComponent,
    children: [

      {
        path: 'dashboard',
        component: DashboardComponent
      }
    ],


  },
  {
    path: '**',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  }
];


@NgModule({
  exports: [
    RouterModule
  ],
  imports: [
    RouterModule.forRoot(appRouteList)
  ]
})
export class AppRoutingModule {

}
