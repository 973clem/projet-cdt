const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: "ad9s9h",

  e2e: {
    experimentalStudio: true,
    baseUrl: 'http://localhost:4200',
    viewportWidth: 1920,
    viewportHeight: 1080,
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
