# Projet CDT
This project is a website named LeMauvaisCoin. It is a website where you can sell and buy items from other users. It is mainly a school project to teach us how to make tests.

## Authors
* Mathis CHARPENTIER
* Jiawei CHEN
* Chloé DUAULT
* Clément DUFAU
* Ellyn LAFONTAINE
* Hugo LIN
* Nikola PLASINTOVIC

## Installation
### 1st step: Pull the project
First of all, you need to pull the project from the repository. You can do it with the following command:
```bash
git clone (the link of the repository)
```
----------

### 2nd step: Install npm and Node.js
Then, you need to install npm and Node.js. Your Node.js version should be at least 14.16 or higher. We recommend you to download and install the latest version of Node.js from the official website: https://nodejs.org/en/download/.

For npm, you can install it with the following command:
```bash
sudo apt install npm
```
----------

### 3rd step: Install the project
Once this is done, you can install the project with the following command:
```bash
npm install -force
```
----------

### 4th step: Run the project
And then, you can run the project with the following command:
```bash
npm start
```

Normally, it should open a new tab in your default browser with the website.

## Description
This website is a platform where you can sell and buy items from other users. It is inspired by the french official website 'leboncoin' [https://www.leboncoin.fr/]. You can create an account, log in, log out, create an ad, delete an ad, edit an ad, see all the ads, see the details of an ad, and make a price offer to the seller. You can also see the profile of a user and see all the ads of a user.

## Technologies
This website is made with the following technologies:
* Cypress for the front-end tests
* Angular (TypeScript, HTML, CSS) for the front-end and the back-end
* Firebase for the database
* Jest for the unit tests
